import React from "react";
import "./leftSidebar.css";
import ultrasoftLogo from "../../assets/ultrasoftLogo.png";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const LeftSidebar = ({ page, setPage }) => {
  const user = useSelector((state) => state.user.currentUser);

  const dummyMenu = [
    {
      id: 1,
      icon: "dashboard",
      title: "Dashboard",
    },
    {
      id: 2,
      icon: "exchange",
      title: "Exchange",
    },
    {
      id: 3,
      icon: "transfer",
      title: "Transfer",
    },
    {
      id: 4,
      icon: "transactions",
      title: "Transactions",
    },
    {
      id: 5,
      icon: "accounting",
      title: "Accounting",
    },
    {
      id: 6,
      icon: "wallet",
      title: "Wallet",
    },
    {
      id: 7,
      icon: "clients",
      title: "Clients",
    },
    {
      id: 8,
      icon: "message",
      title: "Messages",
    },
    {
      id: 9,
      icon: "settings",
      title: "Settings",
    },
  ];

  const dummyIconsTop = [
    {
      id: 1,
      icon: "tether",
    },
    {
      id: 2,
      icon: "bitcoin",
    },
    {
      id: 3,
      icon: "ethereum",
    },
    {
      id: 4,
      icon: "binance-coin",
    },
  ];

  const dummyIconsBottom = [
    {
      id: 1,
      icon: "usd",
    },
    {
      id: 2,
      icon: "euro",
    },
    {
      id: 3,
      icon: "yen",
    },
    {
      id: 4,
      icon: "pound",
    },
  ];

  return (
    <div className="leftSidebar">
      <div className="leftSidebarItem">
        <img
          className="avatar"
          src="https://www.pikpng.com/pngl/m/263-2631740_empty-avatar-png-user-png-clipart.png"
          alt=""
        />
        <Link to="/login" style={{ textDecoration: "none", color: "black" }}>
          <div className="userInfoContainer">
            <span className="userSpan">{user ? user : "Login"}</span>
            <p className="userP">2112-211</p>
          </div>
        </Link>
      </div>
      <div className="leftSidebarItem">
        <div className="leftSidebarWrapper">
          {dummyMenu.map((item) => (
            <div
              key={item.id}
              className="leftSidebarMenuItems"
              onClick={() => setPage(item.title)}
              style={
                page === item.title
                  ? {
                      backgroundColor: "black",
                      color: "white",
                      borderRadius: "10px",
                    }
                  : { backgroundColor: "white", color: "black" }
              }
            >
              <img
                className="leftSidebarMenuIcons"
                src={require(`../../assets/${item.icon}.png`)}
                alt=""
              />
              <span className="leftSidebarMenuSpan">{item.title}</span>
            </div>
          ))}
        </div>
      </div>
      <div className="leftSidebarItem">
        <div className="leftSidebarIconWrapper">
          <div className="iconsTop">
            {dummyIconsTop.map((icon) => (
              <img
                key={icon.id}
                src={require(`../../assets/src/${icon.icon}.png`)}
                alt=""
                className="leftSidebarIcons"
              />
            ))}
          </div>
          <div className="leftSidebarIconsLine" />
          <div className="iconsBottom">
            {dummyIconsBottom.map((icon) => (
              <img
                key={icon.id}
                src={require(`../../assets/${icon.icon}.png`)}
                alt=""
                className="leftSidebarIcons"
              />
            ))}
          </div>
        </div>
      </div>
      <div className="leftSidebarItem">
        <div className="leftSidebarCopyrightWrapper">
          <span className="copyrightSpan">
            <b>Crypto Tresor</b> Version 3.0
          </span>
          <p className="copyrightP">Copyright@2022 Ultrasoft</p>
          <p className="copyrightP">All rights reserved</p>
          <img className="copyrightIcon" src={ultrasoftLogo} alt="" />
        </div>
      </div>
    </div>
  );
};

export default LeftSidebar;
