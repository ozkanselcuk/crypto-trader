import React, { useState } from "react";
// CSS
import "./dashboardSliderFirstRight.css";

const DashboardSliderFirstRight = () => {
  const [transfer, setTransfer] = useState(false);

  const dummySliderData = [
    {
      id: 1,
      currency: "BTC",
      icon: "bitcoin",
      value: 62.401,
    },
    {
      id: 2,
      currency: "USD Tether",
      icon: "tether",
      value: 60.244,
    },
    {
      id: 3,
      currency: "Euro",
      icon: "bitcoin",
      value: 57.218,
    },
  ];

  const apiTry = () => {
    const axios = require("axios");

    let response = null;
    new Promise(async (resolve, reject) => {
      try {
        response = await axios.get(
          "https://pro-api.coinmarketcap.com/v1/cryptocurrency/info",
          {
            headers: {
              "X-CMC_PRO_API_KEY": "5e2d4a34-b216-44d6-827f-0a2f17cd5e10",
            },
            params: {
              symbol:"USDT,BTC"
            },
          }
        );
      } catch (ex) {
        response = null;
        // error
        console.log(ex);
        reject(ex);
      }
      if (response) {
        // success
        const json = response.data;
        console.log(json);
        resolve(json);
      }
    });
  };

  return (
    <div className="dashboardSliderFirstRightContainer">
      <div className="dashboardSliderFirstRightWrapper">
        <div className="dashboardSliderFirstRightCurrency">
          <div className="dashboardSliderFirstRightCurrencyWrapper">
            <span className="dashboardSliderFirstRightCurrencyTopSpan">
              Euro
            </span>
            <span className="dashboardSliderFirstRightCurrencyBottomSpan">
              40,100
            </span>
          </div>
          <div className="dashboardSliderFirstRightCurrencyLine" />
          <div className="dashboardSliderFirstRightCurrencyWrapper">
            <span className="dashboardSliderFirstRightCurrencyTopSpan">
              US Dollar
            </span>
            <span className="dashboardSliderFirstRightCurrencyBottomSpan">
              125,400
            </span>
          </div>
          <div className="dashboardSliderFirstRightCurrencyLine" />
          <div className="dashboardSliderFirstRightCurrencyWrapper">
            <span className="dashboardSliderFirstRightCurrencyTopSpan">
              MK Denar
            </span>
            <span className="dashboardSliderFirstRightCurrencyBottomSpan">
              640,800
            </span>
          </div>
        </div>
        <div className="dashboardSliderFirstRightExchange">
          <div className="dashboardSliderFirstRightExchangeSelectWrapper">
            <select
              name="client"
              id=""
              className="dashboardSliderFirstRightExchangeSelect"
            >
              <option value="imer-selmani">Imer Selmani</option>
              <option value="imer-selmani-2">Imer Selmani 2</option>
            </select>
            <img
              className="dashboardSliderFirstRightExchangeAddIcon"
              src={require("../../../assets/add.png")}
              alt=""
            />
          </div>
          <input
            type="text"
            placeholder="Change"
            className="dashboardSliderFirstRightExchangeChangeInput"
          />
          <div className="dashboardSliderFirstRightExchangeCurrency">
            <div className="dashboardSliderFirstRightExchangeCurrencyLeftwrapper">
              <img
                className="dashboardSliderFirstRightExchangeCurrencyIcon"
                src={require(`../../../assets/src/${
                  transfer ? dummySliderData[0].icon : dummySliderData[1].icon
                }.png`)}
                alt=""
              />
              <div className="dashboardSliderFirstRightExchangeCurrencyInfoContainer">
                <p className="dashboardSliderFirstRightExchangeCurrencyInfoContainerP">
                  You Pay
                </p>
                <span className="dashboardSliderFirstRightExchangeCurrencyInfoContainerSpan">
                  {transfer
                    ? dummySliderData[0].currency
                    : dummySliderData[1].currency}
                </span>
              </div>
            </div>
            <div className="dashboardSliderFirstRightExchangeCurrencyValue">
              <span className="dashboardSliderFirstRightExchangeCurrencyValueSpan">
                {transfer ? dummySliderData[0].value : dummySliderData[1].value}
                ,000
              </span>
            </div>
          </div>
          <div className="dashboardSliderFirstRightExchangeCurrencyTransferContainer">
            <img
              onClick={() => setTransfer(!transfer)}
              src={require("../../../assets/transferCurrency.png")}
              alt=""
              className="dashboardSliderFirstRightExchangeCurrencyTransferContainerIcon"
            />
            <div className="dashboardSliderFirstRightExchangeCurrencyTransferQty">
              <span className="dashboardSliderFirstRightExchangeCurrencyTransferQtySpan">
                Qty:
              </span>
              <div className="dashboardSliderFirstRightExchangeCurrencyTransferQtyValue">
                <span className="dashboardSliderFirstRightExchangeCurrencyTransferQtyValueSpan">
                  1.00
                </span>
              </div>
            </div>
            <div className="dashboardSliderFirstRightExchangeCurrencyTransferFee">
              <span className="dashboardSliderFirstRightExchangeCurrencyTransferFeeSpan">
                Fee:
              </span>
              <div className="dashboardSliderFirstRightExchangeCurrencyTransferFeeValue">
                <span className="dashboardSliderFirstRightExchangeCurrencyTransferFeeValueSpan">
                  2.5%
                </span>
              </div>
            </div>
          </div>
          <div className="dashboardSliderFirstRightExchangeCurrency">
            <div className="dashboardSliderFirstRightExchangeCurrencyLeftwrapper">
              <img
                className="dashboardSliderFirstRightExchangeCurrencyIcon"
                src={require(`../../../assets/src/${
                  transfer ? dummySliderData[1].icon : dummySliderData[0].icon
                }.png`)}
                alt=""
              />
              <div className="dashboardSliderFirstRightExchangeCurrencyInfoContainer">
                <p className="dashboardSliderFirstRightExchangeCurrencyInfoContainerP">
                  You Receive
                </p>
                <span className="dashboardSliderFirstRightExchangeCurrencyInfoContainerSpan">
                  {transfer
                    ? dummySliderData[1].currency
                    : dummySliderData[0].currency}
                </span>
              </div>
            </div>
            <div className="dashboardSliderFirstRightExchangeCurrencyValue">
              <span className="dashboardSliderFirstRightExchangeCurrencyValueSpan">
                {transfer ? dummySliderData[1].value : dummySliderData[0].value}
                ,000
              </span>
            </div>
          </div>
          <img
            onClick={() => apiTry()}
            src={require("../../../assets/transferCurrency.png")}
            alt=""
            className="dashboardSliderFirstRightExchangeCurrencyTransferContainerIconBottom"
          />
          <div className="dashboardSliderFirstRightExchangeCurrency">
            <div className="dashboardSliderFirstRightExchangeCurrencyLeftwrapper">
              <img
                className="dashboardSliderFirstRightExchangeCurrencyIcon"
                src={require(`../../../assets/src/bitcoin.png`)}
                alt=""
              />
              <div className="dashboardSliderFirstRightExchangeCurrencyInfoContainer">
                <p className="dashboardSliderFirstRightExchangeCurrencyInfoContainerP">
                  You Pay
                </p>
                <span className="dashboardSliderFirstRightExchangeCurrencyInfoContainerSpan">
                  Euro
                </span>
              </div>
            </div>
            <div className="dashboardSliderFirstRightExchangeCurrencyValue">
              <span className="dashboardSliderFirstRightExchangeCurrencyValueSpan">
                53,219,000
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashboardSliderFirstRight;
