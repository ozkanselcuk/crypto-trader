import React, { useState } from "react";
import DashboardSliderFirstLeft from "./DashboardSliderFirstLeft/DashboardSliderFirstLeft";
import DashboardSliderFirstRight from "./DashboardSliderFirstRight/DashboardSliderFirstRight";
import "./slider.css";

const Slider = () => {
  const [slide, setSlide] = useState(0);

  return (
    <div className="slider">
      {slide === 0 && (
        <>
          <div className="sliderLeft">
            <DashboardSliderFirstLeft />
          </div>
          <div className="sliderMiddleLine"></div>
          <div className="sliderRight">
            <DashboardSliderFirstRight />
          </div>
        </>
      )}
      {slide === 1 && (
        <>
          <div className="sliderLeft">slide 1 left</div>
          <div className="sliderMiddleLine"></div>
          <div className="sliderRight">slide 1 right</div>
        </>
      )}
      {slide === 2 && (
        <>
          <div className="sliderLeft">slider 2 left</div>
          <div className="sliderMiddleLine"></div>
          <div className="sliderRight">slider 2 right</div>
        </>
      )}
      <div className="sliderButtonWrapper">
        <div
          className="slideButton0"
          onClick={() => setSlide(0)}
          style={
            slide === 0
              ? { backgroundColor: "rgb(141, 0, 0)" }
              : { backgroundColor: "gray" }
          }
        />
        <div
          className="slideButton1"
          onClick={() => setSlide(1)}
          style={
            slide === 1
              ? { backgroundColor: "rgb(141, 0, 0)" }
              : { backgroundColor: "gray" }
          }
        />
        <div
          className="slideButton2"
          onClick={() => setSlide(2)}
          style={
            slide === 2
              ? { backgroundColor: "rgb(141, 0, 0)" }
              : { backgroundColor: "gray" }
          }
        />
      </div>
    </div>
  );
};

export default Slider;
