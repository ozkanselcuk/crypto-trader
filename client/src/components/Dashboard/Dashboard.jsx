import React from "react";
import { LineChart, Line, ResponsiveContainer } from "recharts";
import Slider from "../Slider/Slider";
import "./dashboard.css";

const Dashboard = () => {
  const rechartData = [
    { name: "Page B", uv: 200, pv: 200, amt: 2000 },
    { name: "Page C", uv: 400, pv: 500, amt: 1800 },
    { name: "Page D", uv: 300, pv: 300, amt: 1500 },
    { name: "Page E", uv: 400, pv: 100, amt: 1200 },
    { name: "Page 2", uv: 340, pv: 500, amt: 1800 },
    { name: "Page 3", uv: 360, pv: 300, amt: 1500 },
    { name: "Page 4", uv: 350, pv: 100, amt: 1200 },
    { name: "Page 5", uv: 410, pv: 500, amt: 1800 },
    { name: "Page 6", uv: 400, pv: 300, amt: 1500 },
    { name: "Page 7", uv: 380, pv: 100, amt: 1200 },
    { name: "Page 8", uv: 400, pv: 500, amt: 1800 },
  ];

  const rechart = (
    <ResponsiveContainer width="100%" height={100}>
      <LineChart data={rechartData}>
        <Line dataKey="uv" stroke="#C0392B" dot={false} />
      </LineChart>
    </ResponsiveContainer>
  );

  const headerDummyData = [
    {
      id: 1,
      title: "AVAILABLE",
      subtitle: "USD Tether",
      info: "$67.032",
    },
    {
      id: 2,
      title: "Exchanges",
      subtitle: "Today",
      info: 76,
    },
    {
      id: 3,
      title: "Profit",
      subtitle: "Today",
      info: "$23.135",
    },
    {
      id: 4,
      title: "Clients",
      subtitle: "Today",
      info: 87,
    },
    {
      id: 5,
      title: "Balance",
      subtitle: "Estimate Balance",
      info: "$215.54",
    },
  ];

  const chartDummyData = [
    {
      id: 1,
      title: "TETHER",
      icon: "tether",
      subtitle: "USD Tether",
      change: 6.8,
      sell: 0.99,
      buy: 1.01,
    },
    {
      id: 2,
      title: "BNB",
      icon: "binance-coin",
      subtitle: "Binance Coin",
      change: 1.2,
      sell: 624.69,
      buy: 651.82,
    },
    {
      id: 3,
      title: "BTC",
      icon: "bitcoin",
      subtitle: "Bitcoin",
      change: 6.1,
      sell: 45231.21,
      buy: 47213.56,
    },
    {
      id: 4,
      title: "ETH",
      icon: "ethereum",
      subtitle: "Ethereum",
      change: 4.7,
      sell: 4672.98,
      buy: 4782.2,
    },
    {
      id: 5,
      title: "SOL",
      icon: "solana",
      subtitle: "Solana",
      change: 2.7,
      sell: 252.89,
      buy: 265.87,
    },
  ];

  return (
    <div className="dashboard">
      <div className="dashboardHeader">
        {headerDummyData.map((header) => (
          <div key={header.id} className="dashboardHeaderItems">
            <h4 className="dashboardHeaderTitle">{header.title}</h4>
            <p className="dashboardHeaderSubtitle">{header.subtitle}</p>
            <div className="dashboardHeaderLine"></div>
            <span className="dashboardHeaderInfo">{header.info}</span>
          </div>
        ))}
      </div>
      <div className="dashboardChart">
        <input
          type="text"
          className="dashboardChartInput"
          placeholder="Search"
        />
        <div className="dashboardChartWrapper">
          {chartDummyData.map((item) => (
            <div className="dashboardChartItems">
              <div className="dashboardChartHeader">
                <img
                  src={require(`../../assets/src/${item.icon}.png`)}
                  alt=""
                  className="dashboardChartHeaderIcon"
                />
                <div className="dashboardChartHeaderInfoWrapper">
                  <span className="dashboardChartHeaderTitle">
                    {item.title}
                  </span>
                  <p className="dashboardChartHeaderSubtitle">
                    {item.subtitle}
                  </p>
                </div>
              </div>
              <div className="dashboardChartHeaderChangeWrapper">
                <span className="dashboardChartHeaderChangeTitle">
                  CHANGE (24H)
                </span>
                <p className="dashboardChartHeaderChangeCalc">
                  -{item.change}%
                </p>
              </div>
              <div className="dashboardChartGraph">{rechart}</div>
              <div className="dashboardChartBuySellWrapper">
                <div className="dashboardChartSell">
                  <span className="dashboardChartSellTitle">SELL</span>
                  <p className="dashboardChartSellPrice">{item.sell}</p>
                </div>
                <div className="dashboardChartBuy">
                  <span className="dashboardChartBuyTitle">BUY</span>
                  <p className="dashboardChartBuyPrice">{item.buy}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="dashboardSlider">
        <input
          placeholder="Search"
          type="text"
          className="dashboardSliderInput"
        />
        <div className="dashboardSliderWrapper">
          <Slider />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
