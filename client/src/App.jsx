import React, { useState, useEffect } from "react";
// React Router
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
// Pages
import MainPage from "./pages/MainPage/MainPage";
import LoginPage from "./pages/LoginPage/LoginPage";
// Redux
import { useSelector } from "react-redux";
// Socket.io
import { io } from "socket.io-client";

const App = () => {
  const user = useSelector((state) => state.user.currentUser);
  const [socket, setSocket] = useState(null);

  useEffect(() => {
    setSocket(io("http://localhost:8080"));
  }, []);

  useEffect(() => {
    socket?.emit("newUser", user);
  }, [socket, user]);

  return (
    <BrowserRouter>
      <>
        <Routes>
          <Route path="/" element={user ? <MainPage /> : <LoginPage />} />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </>
    </BrowserRouter>
  );
};

export default App;
