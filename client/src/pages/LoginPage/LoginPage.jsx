import React, { useState } from "react";
// Redux
import { useDispatch } from "react-redux";
import { loginSuccess } from "../../redux/userRedux";
// History
import history from "../../history";
// CSS
import "./loginPage.css";

const LoginPage = () => {
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = () => {
    dispatch(loginSuccess(username, password));

    // history.push("/");
    // history.go(0);
  };

  return (
    <div className="loginPage">
      <form onSubmit={() => handleSubmit()} className="loginPageForm">
        <label className="loginPageFormLabel">Username or Email</label>
        <input
          value={username}
          onChange={(event) => setUsername(event.target.value)}
          type="text"
          className="loginPageFormInput"
          required
        />
        <label className="loginPageFormLabel">Password</label>
        <input
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          type="password"
          className="loginPageFormInput"
          required
        />
        <button className="loginPageFormSubmitButton">Login</button>
      </form>
    </div>
  );
};

export default LoginPage;
