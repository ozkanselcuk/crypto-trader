import React, { useState } from "react";
// Components
import Dashboard from "../../components/Dashboard/Dashboard";
import Exchange from "../../components/Exchange/Exchange";
import Accounting from "../../components/Accounting/Accounting";
import Clients from "../../components/Clients/Clients";
import LeftSidebar from "../../components/LeftSidebar/LeftSidebar";
import RightSidebar from "../../components/RightSidebar/RightSidebar";
import Transactions from "../../components/Transactions/Transactions";
import Transfer from "../../components/Transfer/Transfer";
import Wallet from "../../components/Wallet/Wallet";
import Settings from "../../components/Settings/Settings";
import Messages from "../../components/Messages/Messages";
// CSS
import "./mainpage.css";

const MainPage = () => {
  const [page, setPage] = useState("Dashboard");

  return (
    <div className="container">
      <div className="wrapper">
        <LeftSidebar setPage={setPage} page={page} />
        {page === "Accounting" && <Accounting />}
        {page === "Clients" && <Clients />}
        {page === "Exchange" && <Exchange />}
        {page === "Transactions" && <Transactions />}
        {page === "Transfer" && <Transfer />}
        {page === "Wallet" && <Wallet />}
        {page === "Dashboard" && <Dashboard />}
        {page === "Settings" && <Settings />}
        {page === "Messages" && <Messages />}
        <RightSidebar setPage={setPage} />
      </div>
    </div>
  );
};

export default MainPage;
